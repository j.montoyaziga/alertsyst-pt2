/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PruebaCod1;



import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Joakk
 */
public class ConArduinoTest {

    public ConArduinoTest() {
    }


 
    @Test 
    public void testDataObtainer2CU001() {
        System.out.println("DataObtainer2");
        ConArduino ca = new ConArduino();
        String data = "60;00;00";
        ca.setDataReceiver(data);
        int expResult = 1;
        int result = ca.DataObtainer2(ca.getDataReceiver());
        
        assertEquals(expResult, result);
    }
    
    @Test 
    public void testDataObtainer2CU006() {
        System.out.println("DataObtainer2");
        ConArduino ca = new ConArduino();
        String data = "60;-2;3";
        ca.setDataReceiver(data);
        int expResult = 1;
        int result = ca.DataObtainer2(ca.getDataReceiver());
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testAlertAmarrillaDistCU003(){
        ConArduino ca = new ConArduino();
        String data = "53;00;00";
        ca.setDataReceiver(data);
        int expResult = 1;
        int result = ca.DataObtainer2(ca.getDataReceiver());
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void testAlertRojaDistCU003(){
        
        ConArduino ca = new ConArduino();
        String data = "45;00;00";
        ca.setDataReceiver(data);
        int expResult = 1;
        int result = ca.DataObtainer2(ca.getDataReceiver());
        
        assertEquals(expResult, result);
    }
    
    
    @Test
    public void testAlerAmarrillaInclCU004(){
        ConArduino ca = new ConArduino();
        String data = "60;35;-55";
        ca.setDataReceiver(data);
        int expResult = 1;
        int result = ca.DataObtainer2(ca.getDataReceiver());
        
        assertEquals(expResult, result);
    }

   

    @Test (expected = NullPointerException.class)
    public void testGetInstance() {
        System.out.println("getInstance");
        ConArduino expResult = null;
        ConArduino result = ConArduino.getInstance();
        assertEquals(expResult, result);
    }

   
  


}
