package PruebaCod1;



import BD.ConexionBD;
import Tareas.Proceso.AddDataBD;
import Tareas.Proceso.AlertMsgDist;
import Tareas.Proceso.AlertMsgIncl;
//import Tareas.Proceso.AlertMsgInclinacion;
//import Tareas.Proceso.AlertMsgRio;
import com.panamahitek.ArduinoException;
import java.sql.Connection;
import jssc.SerialPortException;      


public class Init {

    static ConArduino SO;
    public static GraficadorGUI gg;
    public static TableGUI tt;
    
    
    
    public static Object semaforoAlertIncl;
    public static Object semaforoAlertDist;
    public static Integer datosAltura = 62;
    
    //public static Object semaforoRio; 
    //public static Object semaforoInc;
    
    public static Integer datosX = 0;
    public static Integer datosY = 0;
    
    public static Object semaforoBD;
    public static Integer datosH = 62;
    
    
    
    public static ConexionBD cn;
    public static Connection con;

    
    public static void main(String[] args) throws SerialPortException, ArduinoException {
        gg = new GraficadorGUI();
        gg.setVisible(true);
        tt = new TableGUI();
        tt.setVisible(true);

        SO = ConArduino.getInstance();

        SO.arduinoConexion(SO.listener);
        
        
         
        
        //Base de datos
        cn = new ConexionBD();
        con = cn.connectionBD();
         
        

        //semaforoRio = new Object();
        //semaforoInc = new Object();
        semaforoAlertDist = new Object();
        semaforoAlertIncl = new Object();
        semaforoBD = new Object();
        
        //AlertMsgRio hilo = new AlertMsgRio();
        //AlertMsgInclinacion hilo2 = new AlertMsgInclinacion();
        
        AddDataBD hilo3 = new AddDataBD();
        AlertMsgDist hilo4 = new AlertMsgDist();
        AlertMsgIncl hilo5 = new AlertMsgIncl();
        
        //hilo.start();
        //hilo2.start();
        hilo3.start();
        hilo4.start();
        hilo5.start();
        
    }
    
}