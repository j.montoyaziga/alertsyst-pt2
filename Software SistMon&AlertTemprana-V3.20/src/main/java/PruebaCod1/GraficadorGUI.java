package PruebaCod1;

import com.panamahitek.ArduinoException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPortException;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.plot.PlotOrientation;

public class GraficadorGUI extends javax.swing.JFrame {

    //GRAFICO
    final XYSeries dist = new XYSeries("DISTANCIA"); //Serie = se guardan los datos que el sensor HC-SR04 transmite 
    final XYSeries incl = new XYSeries("INCLIANCION"); //se guardan los datos que el sensor GY-521 transmite 
    final XYSeries lvlriver = new XYSeries("NIVEL DEL RIO"); //se guardan los datos del nivel del rio normal
    final XYSeriesCollection coleccionDist = new XYSeriesCollection(); //se agregan las series que participan en el grafico del hc-sr04
    final XYSeriesCollection coleccionIncl = new XYSeriesCollection();//se agregan las series que participan en el grafico del gy-521
    final XYSeriesCollection coleccionNvlRio = new XYSeriesCollection();//se agregan las series que participan en el grafico del nivel del rio
    JFreeChart grafica; //crea el grafico para el dato de distancia
    JFreeChart graficaIncl; //crea el grafico para el dato inclinacion
    JFreeChart graficaNvlRio; //crea el grafico para el nivel del rio
 

    public GraficadorGUI() throws ArduinoException, SerialPortException {
        initComponents();

        coleccionDist.addSeries(dist); //agregar las series a la coleccionDist
        coleccionIncl.addSeries(incl); //agregar las series a la coleccionIncl
        coleccionNvlRio.addSeries(lvlriver); //agregar las series a la coleccionNvlRio
        grafica = ChartFactory.createXYLineChart("Distancia vs Tiempo", "Tiempo (segundos)", "Distancia (cms)", coleccionDist, PlotOrientation.VERTICAL, true, true, false);
        graficaIncl = ChartFactory.createXYLineChart("Estado del dispositivo", "Eje X", "Eje Y", coleccionIncl, PlotOrientation.VERTICAL, true, true, false);
        graficaNvlRio = ChartFactory.createXYLineChart("Nivel del Rio", "Tiempo (segundos)", "Altura del Rio en cm",coleccionNvlRio, PlotOrientation.VERTICAL,true,true,false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Prototipo 1.0");

        jLabel2.setText("Software Receptor de Data");

        jLabel3.setText("Joaquín Montoya");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(jLabel3)))
                .addContainerGap(55, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jLabel2)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addContainerGap(14, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 30, Short.MAX_VALUE)
                    .addComponent(jLabel2)
                    .addGap(0, 31, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GraficadorGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GraficadorGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GraficadorGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GraficadorGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new GraficadorGUI().setVisible(true);
                } catch (ArduinoException | SerialPortException ex) {
                    Logger.getLogger(GraficadorGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    // End of variables declaration//GEN-END:variables

}
