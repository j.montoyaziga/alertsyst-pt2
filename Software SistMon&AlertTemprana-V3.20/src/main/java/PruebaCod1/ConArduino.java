package PruebaCod1;

import com.panamahitek.ArduinoException;
import com.panamahitek.PanamaHitek_Arduino;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

public class ConArduino extends Thread {

    //INSTANCIAR ARDUINO
    PanamaHitek_Arduino ardCon = new PanamaHitek_Arduino();

    //VARIABLES TABLA
    int cont = 1;
    int cont2 = 1;

    //CREAR SINGLETON
    public static ConArduino arduino;
    //String que recibe los datos del Arduino a traves del puerto serial
    private String dataReceiver;
    
    public ConArduino(){
        
    }
    
  
    public String getDataReceiver(){
        return dataReceiver;
    }
    
    public void setDataReceiver(String data){
    this.dataReceiver = data;
    }
   

    //Crear arreglo para almacenar la distancia max y minima
    int still = 0;
    int minDist = 0;
    int nvlAct = 0;

    private final String port = Init.tt.getSelectedPort(); //Especificar el puerto por el cual se transmite los datos
    private final Integer serialPort = Init.tt.getSelectedSerialPort();//Especificar los baudios

    

    public static ConArduino getInstance() {
        if (arduino == null) {
            arduino = new ConArduino();
        } 
        return arduino;
    }

    //CONEXION ARDUINO
    public void arduinoConexion(SerialPortEventListener spe) {

        try {
            ardCon.arduinoRX(port, serialPort, spe);

        } catch (ArduinoException | SerialPortException ex) {
            JOptionPane.showMessageDialog(null, "NO SE ENCUENTRA UN ARDUINO CONECTADO" , "NO DISPONIBLE", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(ConArduino.class.getName()).log(Level.SEVERE, null, ex);
            
        }

    }

 SerialPortEventListener listener = new SerialPortEventListener() {   //listener, esta pendiente de todo lo que pasa en el puerto serial
        @Override
        public void serialEvent(SerialPortEvent serialPortEvent) {      //metodo que se activa cada vez que el arduino recibe datos
            try {
                if (ardCon.isMessageAvailable() == true) {

                    
                    dataReceiver = ardCon.printMessage();
                    if (dataReceiver.equals("No reply, is rf95_server running?")){
                        JOptionPane.showMessageDialog(null, "NO SE TRANSMITIO CORRECTAMENTE", "NO HAY RESPUESTA", JOptionPane.INFORMATION_MESSAGE);
                    } else if (dataReceiver.equals("")) {
                        System.out.println();
                    } else {
                        DataObtainer2(dataReceiver);
                    }
                    
                    /*DataObtainer(1);
                    DataObtainer(2);
                    DataObtainer(3);
                    DataObtainer(4);*/

                } else {
                    //JOptionPane.showMessageDialog(null, "NO HAY MENSAJE DISPONIBLE" , "NO HAY DATO", JOptionPane.ERROR_MESSAGE);
                }
            } catch (SerialPortException | ArduinoException ex) {
                Logger.getLogger(ConArduino.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    };
    
  
    
    public int DataObtainer2(String data) {
        String[] tokens = data.split(";");
        for (String t : tokens) {

            int high = Integer.parseInt(tokens[0]);
            int xs = Integer.parseInt(tokens[1]);
            int ys = Integer.parseInt(tokens[2]);
            Init.tt.tableUpdate(cont++, high);

            Init.gg.dist.add(cont, high); //Agregar al grafico de distancia
            Init.gg.incl.add(xs, ys);//Agregar al graficoIncl

            synchronized(Init.semaforoAlertDist){
                Init.datosAltura = high;
                Init.semaforoAlertDist.notify();
            }
            
            synchronized (Init.semaforoAlertIncl) {
                Init.datosX = xs;
                Init.datosY = ys;
                Init.semaforoAlertIncl.notify();
            }
            
            
                    
            synchronized (Init.semaforoBD) {
                Init.datosH = high;
                Init.semaforoBD.notify();
            }

            minDist = high;
            
            if ((high == minDist && nvlAct == 0) || minDist == nvlAct) {
                Init.gg.lvlriver.add(cont2++, minDist);

                
                nvlAct = minDist;
                still = nvlAct;
                
            } else if (nvlAct > minDist) {
                if(nvlAct == still){
                    nvlAct = nvlAct + ( nvlAct - minDist);
                    Init.gg.lvlriver.add(cont2++,nvlAct);
                    still = minDist;
                }else if (minDist == still){
                    Init.gg.lvlriver.add(cont2++,nvlAct);
                    still = minDist;
                } else if (minDist < still){
                    nvlAct = nvlAct + (still - minDist);
                    Init.gg.lvlriver.add(cont2++,nvlAct);
                    still = minDist;
                } else if (minDist > still){
                    nvlAct = nvlAct - (minDist - still);
                    Init.gg.lvlriver.add(cont2++,nvlAct);
                    still = minDist;
                    
                }
            } else if (nvlAct < minDist) {
                if(nvlAct == still){
                    nvlAct = nvlAct +    ( nvlAct - minDist);
                    Init.gg.lvlriver.add(cont2++,nvlAct);
                    still = minDist;
                }else if (minDist == still){
                    Init.gg.lvlriver.add(cont2++,nvlAct);
                    still = minDist;
                } else if (minDist < still){
                    nvlAct = nvlAct + (still - minDist);
                    Init.gg.lvlriver.add(cont2++,nvlAct);
                    still = minDist;
                } else if (minDist > still){
                    nvlAct = nvlAct - (minDist - still);
                    Init.gg.lvlriver.add(cont2++,nvlAct);
                    still = minDist;
                    
                }

            }
            
            break;

        }
        return 1;
        
    }

    
    
    /*int distMaxCP1 = 99;
    int distMinCP1 = 60;

    public String testDistancia() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i == 10; i++) {
            int dist = (int) (Math.random() * (distMinCP1 - (distMaxCP1 + 1) + distMaxCP1));
            sb.append(dist);
        }
        
        return sb.toString() + ";00;00";

    }*/
    /*public void dataReceiver() {
        for(int i = 0; i== 10; i++){
            
        }
        dataReceiver = testDistancia();
        DataObtainer2(dataReceiver);
    }*/

    //VERSION ANTERIOR DATA OBTAINER
    /*public void DataObtainer(int op) throws SerialPortException, ArduinoException {
        String[] tokens = dataReceiver.split(";");

        switch (op) {
            case 1:
                //LLENAR LA TABLA
                for (String t : tokens) {
                    Init.tt.tableUpdate(cont++, Integer.parseInt(tokens[0]));
                }
                break;
            case 2:
                //MOSTRAR DISTANCIA 
                for (String t : tokens) {

                    Init.gg.i++;
                    Init.gg.dist.add(Init.gg.i, Integer.parseInt(tokens[0]));
                    synchronized (Init.semaforoRio) {
                        Init.datosAltura = Integer.parseInt(tokens[0]);
                        Init.semaforoRio.notify();
                    }

                }
                break;
            case 3:
                //MOSTRAR INCLINACION
                for (String t : tokens) {
                    Init.gg.incl.add(Double.parseDouble(tokens[1]), Double.parseDouble(tokens[2]));
                    
                    synchronized(Init.semaforoInc){
                        Init.datosX = Double.parseDouble(tokens[1]);
                        Init.datosY = Double.parseDouble(tokens[2]);
                        Init.semaforoInc.notify();
                    }
                }
                break;
                
            case 4:
                synchronized (Init.semaforoBD) {
                        Init.datosH = Integer.parseInt(tokens[0]);
                        Init.semaforoBD.notify();
                    }
                break;
        }
    }*/
}
