/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PruebaSensores;

import PruebaCod1.ConArduino;
import com.panamahitek.ArduinoException;
import com.panamahitek.PanamaHitek_Arduino;
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

/**
 *
 * @author Joakk
 */
public class Pr_SingleObject {

    //INSTANCIAR ARDUINO
    PanamaHitek_Arduino ardCon = new PanamaHitek_Arduino();

    //VARIABLES TABLA
    int cont = 0;

    //CREAR SINGLETON
    public static Pr_SingleObject arduino;

    private Pr_SingleObject() {

    }

    public static Pr_SingleObject getInstance() {
        if (arduino == null) {
            arduino = new Pr_SingleObject();
        }
        return arduino;
    }

    //CONEXION ARDUINO
    public void arduinoConexion(SerialPortEventListener spe) {

        try {
            ardCon.arduinoRX("COM3", 9600, spe);
        } catch (ArduinoException | SerialPortException ex) {
            Logger.getLogger(ConArduino.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private String dataReceiver = "";

    SerialPortEventListener listener = new SerialPortEventListener() {   //listener, esta pendiente de todo lo que pasa en el puerto serial
        @Override
        public void serialEvent(SerialPortEvent serialPortEvent) {      //metodo que se activa cada vez que el arduino recibe datos
            try {
                if (ardCon.isMessageAvailable() == true) {

                    dataReceiver = ardCon.printMessage();
               
                    DataObtainer(1);

                }
            } catch (SerialPortException | ArduinoException ex) {
                Logger.getLogger(ConArduino.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    };

    public void DataObtainer(int op) throws SerialPortException, ArduinoException {

        switch (op) {
            case 1: //LLENAR LA TABLA
                String[] tokens = dataReceiver.split(";");
                for(String t: tokens){
                    System.out.println(t);
                }
                //https:stackoverflow.com/questions/3760152/split-string-to-equal-length-substrings-in-java
                //System.out.println(Array.toString(dataReceiver.split("(?<=\\G.{4})")));
                //Inif.tt.tableUpdate());
                /*for(final String token :
                        Splitter.fixedLength(4).split(dataReceiver))
                    System.out.println(token);*/
                //https://www.tutorialspoint.com/guava/guava_spliter.htm
                break;
            case 2:
                //https://stackoverflow.com/questions/14833008/java-string-split-with-dot
                
                break;

        }
    }

}
