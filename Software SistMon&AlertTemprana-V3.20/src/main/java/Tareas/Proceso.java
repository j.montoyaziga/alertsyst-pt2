/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tareas;

import PruebaCod1.Init;
import static PruebaCod1.Init.datosAltura;
import static PruebaCod1.Init.datosH;
import static PruebaCod1.Init.datosX;
import static PruebaCod1.Init.datosY;
import static PruebaCod1.Init.semaforoAlertIncl;
import static PruebaCod1.Init.semaforoAlertDist;
import static PruebaCod1.Init.semaforoBD;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
//import static PruebaCod1.Init.semaforoInc;
//import static PruebaCod1.Init.semaforoRio;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

/**
 *
 * @author Joakk
 */
public class Proceso {

    /*public static class AlertMsgRio extends Thread {

        @Override
        public void run() {
            while (true) {
                try {
                    if (datosAltura <= 10) {

                        JOptionPane.showMessageDialog(null, "NIVEL DEL RIO A " + datosAltura + "CM DEL SENSOR", "ALERTA ROJA", JOptionPane.WARNING_MESSAGE);

                    } else if (datosAltura >= 11 && datosAltura <= 30) {
                        JOptionPane.showMessageDialog(null, "CRECIDA DEL NIVEL DEL RIO PELIGROSA", "ALERTA AMARRILLA", JOptionPane.WARNING_MESSAGE);

                    }

                    synchronized (semaforoRio) {
                        semaforoRio.wait();
                    }

                } catch (InterruptedException ex) {
                    Logger.getLogger(Init.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        public static void main(String ars[]) {
            (new AlertMsgRio()).start();
        }
    }

    public static class AlertMsgInclinacion extends Thread {

        @Override
        public void run() {
            while (true) {
                try {
                    if (datosX >= 5 || datosX <= -5) {

                        JOptionPane.showMessageDialog(null, "INCLINACION PELIGROSA DEL DISPOSITIVO\n" + "DISPOSITIVO SE ENCUENTRA A " + datosX + "° EN X " + datosY + "° EN Y", "ALERTA AMARRILLA", JOptionPane.WARNING_MESSAGE);

                    } else if (datosY >= 5 || datosY <= -5) {
                        JOptionPane.showMessageDialog(null, "INCLINACION PELIGROSA DEL DISPOSITIVO\n" + "DISPOSITIVO SE ENCUENTRA A " + datosX + "° EN X " + datosY + "° EN Y", "ALERTA ROJA", JOptionPane.WARNING_MESSAGE);

                    }

                    synchronized (semaforoInc) {
                        semaforoInc.wait();
                    }

                } catch (InterruptedException ex) {
                    Logger.getLogger(Init.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }*/
    public static class AlertMsgIncl extends Thread {

        @Override
        public void run() {
            while (true) {
                try {
                    if (datosX >= 5 || datosX <= -5) {

                        JOptionPane.showMessageDialog(null, "INCLINACION PELIGROSA DEL DISPOSITIVO\n" + "DISPOSITIVO SE ENCUENTRA A " + datosX + "° EN X " + datosY + "° EN Y", "ALERTA AMARRILLA", JOptionPane.WARNING_MESSAGE);

                    } else if (datosY >= 5 || datosY <= -5) {
                        JOptionPane.showMessageDialog(null, "INCLINACION PELIGROSA DEL DISPOSITIVO\n" + "DISPOSITIVO SE ENCUENTRA A " + datosX + "° EN X " + datosY + "° EN Y", "ALERTA ROJA", JOptionPane.WARNING_MESSAGE);

                    }

                    synchronized (semaforoAlertIncl) {
                        semaforoAlertIncl.wait();
                    }

                } catch (InterruptedException ex) {
                    Logger.getLogger(Init.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }

    public static class AlertMsgDist extends Thread {

        @Override
        public void run() {
            while (true) {
                try {
                    if (datosAltura < 50) {

                        JOptionPane.showMessageDialog(null, "NIVEL DEL RIO A " + datosAltura + "CM DEL SENSOR", "ALERTA ROJA", JOptionPane.WARNING_MESSAGE);
                        InputStream music;
                        try {
                            music = new FileInputStream(new File("C:\\Users\\Joakk\\Documents\\Proyecto de titulo 2\\alertsyst-pt2\\NukeAlertSound.wav"));
                            AudioStream audio = new AudioStream(music);
                            AudioPlayer.player.start(audio);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e.getLocalizedMessage());
                        }

                    } else if (datosAltura >= 50 && datosAltura <= 55) {
                        JOptionPane.showMessageDialog(null, "CRECIDA DEL NIVEL DEL RIO PELIGROSA", "ALERTA AMARRILLA", JOptionPane.WARNING_MESSAGE);

                    }

                    synchronized (semaforoAlertDist) {
                        semaforoAlertDist.wait();
                    }

                } catch (InterruptedException ex) {
                    Logger.getLogger(Init.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public static class AddDataBD extends Thread {

        PreparedStatement pps;

        @Override
        public void run() {
            while (true) {
                try {
                    pps = Init.con.prepareStatement("INSERT INTO datareceiver(Distance, TimeReception) VALUES(?,?)");
                    pps.setInt(1, datosH);
                    pps.setString(2, Init.tt.timeObtainer());
                    pps.executeUpdate();

                    synchronized (semaforoBD) {
                        semaforoBD.wait();
                    }

                } catch (InterruptedException ex) {
                    Logger.getLogger(Init.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(Proceso.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }

}
