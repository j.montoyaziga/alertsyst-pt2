//GY-521 & HC-SR04
#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"

#define Pecho 8
#define Ptrig 9
long duracion, distancia;
MPU6050 sensor;
int ax, ay, az;

char cadena[11], num[5] =  ""; 

//Grove LoRa 433MHz
#include <SoftwareSerial.h>
#include <RH_RF95.h>
// Singleton instance of the radio driver
SoftwareSerial ss(5, 6);
RH_RF95 rf95(ss);


void setup() {
 Serial.begin(115200);     // inicializaz el puerto serial a 9600 baudios
  Wire.begin();           //Iniciando I2C  
  sensor.initialize();    //Iniciando el sensor
  pinMode(Pecho, INPUT);  // define el pin 6 como entrada (echo) 
  pinMode(Ptrig, OUTPUT); // define el pin 7 como salida (trigger)
  pinMode(13, 1);         // define el pin 13 como salida

  if (!rf95.init())
    {
        Serial.println("init failed");
        while(1);
    }

    rf95.setFrequency(434.0);

}

void loop() {
  digitalWrite(Ptrig, LOW); // verificar si esta apagado
  delayMicroseconds(2);
  digitalWrite(Ptrig, HIGH); // generar pulso de trigger a 10ms
  delayMicroseconds(10);
  digitalWrite(Ptrig, LOW); // apagar

  duracion = pulseIn(Pecho,HIGH); // cuanto dura el pulso de ida y retorno
  distancia = (int)((duracion/2)/29);    //calcula la distancia en cm y se le resta el largo del flotador


  /*if (distancia >= 500 || distancia <= 0){ // si la distancia es mayor a 500cm o menor a 0cm
    String dataH = "0;";
    Serial.println("0");                // no mide nada
    Serial.println(";");
  } else {
     Serial.println((distancia)); // envia el valor de la distancia al puerto serial
     Serial.println(";");
  }*/

  sensor.getAcceleration(&ax, &ay, &az);
  //Calcular los angulos de inclinacion:
  int accel_ang_x=(int)(atan(ax/sqrt(pow(ay,2) + pow(az,2)))*(180.0/3.14));
  int accel_ang_y=(int)(atan(ay/sqrt(pow(ax,2) + pow(az,2)))*(180.0/3.14));
  //Mostrar los angulos separadas por un [tab]
  
 /*if (distancia >= 500 || distancia <= 0){
    dato = zero+ String(accel_ang_x) + ";" + String(accel_ang_y);
 }else{
  if(dato.length() == 5){
    dato= String(distancia) + ";" + String(accel_ang_x) + ";" + String(accel_ang_y);
    datoTotal[5] = {&dato[0u]};
    rf95.send(datoTotal, sizeof(datoTotal));
    rf95.waitPacketSent();
  }else if(dato.length() == 6){
    dato= String(distancia) + ";" + String(accel_ang_x) + ";" + String(accel_ang_y);
    datoTotal[6] = {&dato[0u]};
    rf95.send(datoTotal, sizeof(datoTotal));
    rf95.waitPacketSent();
  }else if(dato.length() == 7){
    dato= String(distancia) + ";" + String(accel_ang_x) + ";" + String(accel_ang_y);
    datoTotal[7] = {&dato[0u]};
    rf95.send(datoTotal, sizeof(datoTotal));
    rf95.waitPacketSent();
  }else if(dato.length() == 8){
    dato= String(distancia) + ";" + String(accel_ang_x) + ";" + String(accel_ang_y);
    datoTotal[8] = {&dato[0u]};
    rf95.send(datoTotal, sizeof(datoTotal));
    rf95.waitPacketSent();
  }else if(dato.length() == 9){
    dato= String(distancia) + ";" + String(accel_ang_x) + ";" + String(accel_ang_y);
    datoTotal[9] = {&dato[0u]};
    rf95.send(datoTotal, sizeof(datoTotal));
    rf95.waitPacketSent();
  }else if(dato.length() == 10){
    dato= String(distancia) + ";" + String(accel_ang_x) + ";" + String(accel_ang_y);
    datoTotal[10] = {&dato[0u]};
    rf95.send(datoTotal, sizeof(datoTotal));
    rf95.waitPacketSent();
  }else if(dato.length() ==11){
    dato= String(distancia) + ";" + String(accel_ang_x) + ";" + String(accel_ang_y);
    datoTotal[11] = {&dato[0u]};
    rf95.send(datoTotal, sizeof(datoTotal));
    rf95.waitPacketSent();
  }
    
 }*/

  sprintf(num, "%d", distancia);
  strcat(strcat(cadena, num), ";");
  sprintf(num, "%d", accel_ang_x);
  strcat(strcat(cadena, num), ";");
  sprintf(num, "%d", accel_ang_y); 
  strcat(cadena, num);

  //Serial.println(cadena);
  //printf("%s\n", cadena);
  
  //char dato[11] = {static_cast<char>(distancia) + ';' + static_cast<char>(accel_ang_x) + ';' + static_cast<char>(accel_ang_y)};
  rf95.send(cadena, sizeof(cadena));

   rf95.waitPacketSent();
  //Serial.println(dato); 

  // Now wait for a reply
    /*uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);

    if(rf95.waitAvailableTimeout(3000))
    {
        // Should be a reply message for us now   
        if(rf95.recv(buf, &len))
        {
            Serial.print("got reply: ");
            Serial.println((char*)buf);
        }
        else
        {
            Serial.println("recv failed");
        }
    }
    else
    {
        Serial.println("No reply, is rf95_server running?");
    }*/
    delay(1000);

}
