#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"

#define Pecho 8
#define Ptrig 9
long duracion, distancia;
MPU6050 sensor;
int ax, ay, az;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);     // inicializaz el puerto serial a 9600 baudios
  Wire.begin();           //Iniciando I2C  
  sensor.initialize();    //Iniciando el sensor
  pinMode(Pecho, INPUT);  // define el pin 6 como entrada (echo) 
  pinMode(Ptrig, OUTPUT); // define el pin 7 como salida (trigger)
  pinMode(13, 1);         // define el pin 13 como salida
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(Ptrig, LOW); // verificar si esta apagado
  delayMicroseconds(2);
  digitalWrite(Ptrig, HIGH); // generar pulso de trigger a 10ms
  delayMicroseconds(10);
  digitalWrite(Ptrig, LOW); // apagar

  duracion = pulseIn(Pecho,HIGH); // cuanto dura el pulso de ida y retorno
  distancia = ((duracion/2)/29);    //calcula la distancia en cm y se le resta el largo del flotador


  if (distancia >= 500 || distancia <= 0){ // si la distancia es mayor a 500cm o menor a 0cm
    Serial.println("0");                // no mide nada
    Serial.println(";");
  } else {
     Serial.println((distancia)); // envia el valor de la distancia al puerto serial
     Serial.println(";");
  }

  sensor.getAcceleration(&ax, &ay, &az);
  //Calcular los angulos de inclinacion:
  float accel_ang_x=atan(ax/sqrt(pow(ay,2) + pow(az,2)))*(180.0/3.14);
  float accel_ang_y=atan(ay/sqrt(pow(ax,2) + pow(az,2)))*(180.0/3.14);
  //Mostrar los angulos separadas por un [tab]
  Serial.println(accel_ang_x); 
  Serial.println(";");
  Serial.println(accel_ang_y);
  delay(2000);
}
