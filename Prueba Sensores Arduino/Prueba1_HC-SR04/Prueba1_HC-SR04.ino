

#define Pecho 6
#define Ptrig 7
long duracion, distancia;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);     // inicializaz el puerto serial a 9600 baudios
  pinMode(Pecho, INPUT);  // define el pin 6 como entrada (echo) 
  pinMode(Ptrig, OUTPUT); // define el pin 7 como salida (trigger)
  pinMode(13, 1);         // define el pin 13 como salida
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(Ptrig, LOW); // verificar si esta apagado
  delayMicroseconds(2);
  digitalWrite(Ptrig, HIGH); // generar pulso de trigger a 10ms
  delayMicroseconds(10);
  digitalWrite(Ptrig, LOW); // apagar

  duracion = pulseIn(Pecho,HIGH); // cuanto dura el pulso de ida y retorno
  distancia = ((duracion/2)/29);    //calcula la distancia en cm y se le resta el largo del flotador


  if (distancia >= 500 || distancia <= 0){ // si la distancia es mayor a 500cm o menor a 0cm
    Serial.println("0");                // no mide nada
  } else {
     Serial.println((distancia)); // envia el valor de la distancia al puerto serial
  }

  delay(400);
}
